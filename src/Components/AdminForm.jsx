import React, { useState } from 'react'

const AdminForm = () => {

  const [admin, setAdmin] = useState({
    nombre: "",
    apellidos: "",
    email: "",
    usuario: "",
    pass: ""
  })

  const handleChange = e => {
    setAdmin({
      ...admin,
      [e.target.name]: e.target.value
    })
  }

  const handleSubmit = () => {
    const requestInit = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(admin)
    }
    fetch('http://localhost:5050/admin', requestInit)
      .then(res => res.text())
      .then(res => console.log(res))

    setAdmin({
      nombre: '',
      apellidos: "",
      email: "",
      usuario: "",
      pass: ""
    })
  }

  return (
    <div className='app_admin'>
      <div className="app_admin-container">
        <div className="app_admin-box">
          <h1>Agregar un Nuevo Administrador</h1>
          <form onSubmit={handleSubmit}>
            <label htmlFor="nombre">Nombre: </label>
            <input name="nombre" value={admin.nombre} onChange={handleChange} type="text" placeholder='Nombre del nuevo administrador' id="nombre" required />
            <label htmlFor="apellidos">Apellidos: </label>
            <input value={admin.apellidos} onChange={handleChange} name="apellidos" type="text" placeholder='Apellidos del nuevo administrador' id="apellidos" required/>
            <label htmlFor="email">E-mail: </label>
            <input value={admin.email} onChange={handleChange} name="email" type="email" placeholder='Email del nuevo administrador' id="email" required/>
            <label htmlFor="usuario">Nombre de usuario: </label>
            <input value={admin.usuario} onChange={handleChange} name="usuario" type="text" placeholder='Nombre de usuario administrador' id="usuario" required/>
            <label htmlFor="pass">Contraseña: </label>
            <input value={admin.pass} onChange={handleChange} name="pass" type="password" placeholder='Contraseña del nuevo administrador' id="usuario" required/>
            <button type='submit'>Registrar</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AdminForm
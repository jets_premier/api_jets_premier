import React from 'react'
import JetsPremier from '../assets/logo_black.png';

import './components.css'

const Navbar = () => {
  return (
    <div className="app_navbar">
        <img src={JetsPremier} alt="jets-premier-logo" />
    </div>
  )
}

export default Navbar
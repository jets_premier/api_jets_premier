import React, { useState } from 'react'

const ChoferesForm = () => {

  const [chofer, setChofer] = useState({
    nombre: '',
    apellidos: '',
    email: '',
    telefono: 0
  })

  const handleChange = e => {
    setChofer({
      ...chofer,
      [e.target.name]: e.target.value
    })
  }

  const handleSubmit = () => {
    chofer.telefono = parseInt(chofer.telefono, 10)

    const requestInit = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(chofer)
    }
    fetch('http://localhost:5050/choferes', requestInit)
      .then(res => res.text())
      .then(res => console.log(res))

    setChofer({
      nombre: '',
      apellidos: '',
      email: '',
      telefono: 0
    })
  }

  return (
    <div className='app_choferes'>
      <div className='app_choferes-container'>
        <div className="app_choferes-box">
          <h1>Agregar nuevo Chofer</h1>
          <form onSubmit={handleSubmit}>
            <label htmlFor="nombre">Nombre: </label>
            <input value={chofer.nombre} onChange={handleChange} name="nombre" type="text" placeholder='Nombre del nuevo chofer' id="nombre" required />
            <label htmlFor="apellidos">Apellidos: </label>
            <input value={chofer.apellidos} onChange={handleChange} name="apellidos" type="text" placeholder='Apellidos del nuevo chofer' id="apellidos" required />
            <label htmlFor="email">E-mail: </label>
            <input value={chofer.email} onChange={handleChange} name="email" type="text" placeholder='E-mail del nuevo chofer' id="email" required />
            <label htmlFor="telefono">Telefono: </label>
            <input value={chofer.telefono} onChange={handleChange} name="telefono" type="number" placeholder='Telefono del nuevo chofer' id="telefono" required />
            <button type='submit'>Registrar</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ChoferesForm
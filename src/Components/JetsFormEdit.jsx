import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import NavbarV from '../Components/Navbar-V'

const URI = 'http://localhost:5050/jets/'

const JetsFormEdit = () => {
    const [nombre, setNombre] = useState('')
    const [descripcion, setDescripcion] = useState('')
    const [precio, setPrecio] = useState('')

    const navigate = useNavigate()
    const {id} = useParams()

    const update = async (e) => {
        e.preventDefault()
        await axios.put(URI+id, {
            nombre: nombre,
            descripcion: descripcion,
            precio: precio
        })
        navigate('/jets-list')
    }

    useEffect(()=>{
        getJetById()
          // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

    const getJetById = async () => {
        const res = await axios.get(URI+id)
        setNombre(res.data.nombre)
        setDescripcion(res.data.descripcion)
        setPrecio(res.data.precio)
    }

    return (
        <div className='app_jets'>
            <NavbarV />
            <div className='app_jets-container'>
                <div className='app_jets-box'>
                    <h1>Actualizar Jet</h1>
                    <form onSubmit={update}>
                        <label htmlFor="nombre">Nombre: </label>
                        <input value={nombre} onChange={(e)=> setNombre(e.target.value)} name="nombre" type="text" placeholder='Titulo del Jet' id="nombre" required />
                        <label htmlFor="descripcion">Descripcion: </label>
                        <input value={descripcion} onChange={(e)=> setDescripcion(e.target.value)} name="descripcion" id="descripcion" type="text" placeholder='Desc del Jet' required />
                        <label htmlFor="precio">Precio por día: </label>
                        <input value={precio} onChange={(e)=> setPrecio(e.target.value)} name="precio" id="precio" type="number" required />
                        <button type='submit'>Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default JetsFormEdit
import React from 'react'
import { NavLink, Link } from 'react-router-dom'

import JetsPremier from '../assets/logo_black.png';

import './components.css'

const NavbarV = () => {
    return (
        <div className='app__navbarV'>
            <div className="app__navbarV-container">
                <img src={JetsPremier} alt="jets-premier-logo" />
                <div className="app__navbarV-item">
                    <NavLink to="/admin">ADMIN</NavLink>
                    <div className='sub-item'>
                    <Link to="/admin">
                            <div>Agregar un nuevo administrador</div>
                    </Link> 
                    <Link to="/admin-list">
                            <div>Ver lista de administradores</div>
                    </Link>
                    </div>
                </div>
                <div className="app__navbarV-item">
                    <NavLink to="/jets">JETS</NavLink>
                    <div className='sub-item'>
                        <Link to="/jets">
                            <div>Agregar un nuevo Jet</div>
                        </Link>
                        <Link to="/jets-list">
                        <div>Ver lista de jets</div>
                        </Link>
                    </div>
                </div>
                <div className="app__navbarV-item">
                    <NavLink to="/choferes">CHOFERES</NavLink>
                    <div className='sub-item'>
                        <Link to="/choferes">
                            <div>Agregar un nuevo chofer</div>
                        </Link>
                        <Link to="/choferes-list">
                        <div>Ver lista de choferes</div>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NavbarV
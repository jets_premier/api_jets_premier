import React, {useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const JetsForm = () => {

    const [jet, setJet] = useState({
        nombre: '',
        descripcion: '',
        precio: 0
    })

    const handleChange = e => {
        setJet({
            ...jet,
            [e.target.name]: e.target.value
        })
    }

    // let { nombre, descripcion, precio } = jet

    const handleSubmit = () => {
        jet.precio = parseInt(jet.precio, 10)

        const requestInit = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jet)
        }
        fetch('http://localhost:5050/jets', requestInit)
            .then(res => res.text())
            .then(res => console.log(res))

        setJet({
            nombre: '',
            descripcion: '',
            precio: 0
        })
    }

    return (
        <div className='app_jets'>
            <div className='app_jets-container'>
                <div className='app_jets-box'>
                    <h1>Agregar un Nuevo Jet</h1>
                    <form onSubmit={handleSubmit}>
                    <label htmlFor="nombre">Nombre: </label>
                    <input value={jet.nombre} onChange={handleChange} name="nombre" type="text" placeholder='Titulo del Jet' id="nombre" required />
                    <label htmlFor="descripcion">Descripcion: </label>
                    <input value={jet.descripcion} onChange={handleChange} name="descripcion" id="descripcion" type="text" placeholder='Desc del Jet' required />
                    <label htmlFor="precio">Precio por día: </label>
                    <input onChange={handleChange} name="precio" value={jet.precio} id="precio" type="number" required />
                    <button type='submit'>Registrar</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default JetsForm
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'

import Home from './Pages/Home';
import Login from './Pages/Login';

import Choferes from './Pages/Choferes';
import Jets from './Pages/Jets';
import Admin from './Pages/Admin';
import ChoferesList from './Pages/ChoferesList';
import AdminList from './Pages/AdminList';
import JetsList from './Pages/JetsList';

import JetsFormEdit from './Components/JetsFormEdit'


function App() {
  return (
    <BrowserRouter>
      <div>
        <Routes>
          <Route path="/" element={<Login />}/>
          <Route path="/home" element={<Home />}/>

          <Route path="/choferes" element={<Choferes />}/>
          <Route path="/jets" element={<Jets />}/>
          <Route path="/admin" element={<Admin />}/>

          <Route path="/choferes-list" element={<ChoferesList />}/>
          <Route path="/jets-list" element={<JetsList />}/>
          <Route path="/admin-list" element={<AdminList />}/>

          <Route path="/jets/:id" element={<JetsFormEdit />}/>
          <Route />

        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;

import React from 'react'

import NavbarV from '../Components/Navbar-V'
import ChoferesForm from '../Components/ChoferesForm'

import './pages.css'

const Choferes = () => {
  return (
    <div className='app_choferes'>
      <NavbarV />
      <ChoferesForm />
    </div>
  )
}

export default Choferes
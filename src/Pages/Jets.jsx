import React from 'react'
import JetsForm from '../Components/JetsForm'
import NavbarV from '../Components/Navbar-V'

import './pages.css'

const Jets = () => {
  return (
    <div className='app_jets'>
      <NavbarV />
      <JetsForm />
    </div>
  )
}

export default Jets
import React, { useEffect, useState } from 'react'
import NavbarV from '../Components/Navbar-V'

import './pages.css'

const AdminList = () => {

  const [admins, setAdmins] = useState([])

  const [listUpdate, setListUpdate] = useState(false)

  useEffect(() => {
    const getAdmins = () => {
      fetch('http://localhost:5050/admin')
        .then(res => res.json())
        .then(res => setAdmins(res))
    }
    getAdmins()
    setListUpdate(false)
  }, [listUpdate])

  const handleDelete = (id) => {
    const requestInit = {
      method: 'DELETE'
    }
    fetch('http://localhost:5050/admin/' + id, requestInit)
      .then(res => res.text())
      .then(res => console.log(res))

      setListUpdate(true)
  }

  return (
    <div className='app_jets'>
      <NavbarV />
      <div className='app_table'>
        <table className='content-table'>
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>Email</th>
              <th>Nom. Usuario</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {
              admins.map(admin => (
                <tr key={admin.id}>
                  <td>{admin.id}</td>
                  <td>{admin.nombre}</td>
                  <td>{admin.apellidos}</td>
                  <td>{admin.email}</td>
                  <td>{admin.usuario}</td>
                  <td>
                    <button onClick={() => handleDelete(admin.id)} className='btn_delete'>Eliminar</button>
                    <button className='btn_edit'>Editar</button>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default AdminList
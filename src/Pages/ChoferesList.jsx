import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import NavbarV from '../Components/Navbar-V'

import './pages.css'

const ChoferesList = () => {

  const [choferes, setChoferes] = useState([])

  const [listUpdate, setListUpdate] = useState(false)

  useEffect(() => {
    const getChoferes = () => {
      fetch('http://localhost:5050/choferes')
        .then(res => res.json())
        .then(res => setChoferes(res))
    }
    getChoferes()
    setListUpdate(false)
  }, [listUpdate])

  const handleDelete = (id) => {
    const requestInit = {
      method: 'DELETE'
    }
    fetch('http://localhost:5050/choferes/' + id, requestInit)
      .then(res => res.text())
      .then(res => console.log(res))

    setListUpdate(true)
  }

  return (
    <div className='app_jets'>
      <NavbarV />
      <div className='app_table'>
        <table className='content-table'>
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>Email</th>
              <th>Telefono</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {
              choferes.map(chofer => (
                <tr key={chofer.id}>
                  <td>{chofer.id}</td>
                  <td>{chofer.nombre}</td>
                  <td>{chofer.apellidos}</td>
                  <td>{chofer.email}</td>
                  <td>{chofer.telefono}</td>
                  <td>
                    <button onClick={() => handleDelete(chofer.id)} className='btn_delete'>Eliminar</button>
                    <Link>
                      <button className='btn_edit'>Editar</button>
                    </Link>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default ChoferesList
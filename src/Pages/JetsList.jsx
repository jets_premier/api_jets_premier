import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import NavbarV from '../Components/Navbar-V'

import './pages.css'

const JetsList = () => {

  const [jets, setJets] = useState([])

  const [listUpdate, setListUpdate] = useState(false)

  useEffect(() => {
    const getBooks = () => {
      fetch('http://localhost:5050/jets')
        .then(res => res.json())
        .then(res => setJets(res))
    }
    getBooks()
    setListUpdate(false)
  }, [listUpdate])

  const handleDelete = (id) => {
    const requestInit = {
      method: 'DELETE'
    }
    fetch('http://localhost:5050/jets/' + id, requestInit)
      .then(res => res.text())
      .then(res => console.log(res))

    setListUpdate(true)
  }

  return (
    <div className='app_jets'>
      <NavbarV />
      <div className='app_table'>
        <table className='content-table'>
          <thead>
            <tr>
              <th>ID</th>
              <th>NOMBRE</th>
              <th>DESCRIPCION</th>
              <th>PRECIO x DÍA</th>
              <th>ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            {
              jets.map(jet => (
                <tr key={jet.id}>
                  <td>{jet.id}</td>
                  <td>{jet.nombre}</td>
                  <td>{jet.descripcion}</td>
                  <td>${jet.precio}</td>
                  <td>
                    <button onClick={() => handleDelete(jet.id)} className='btn_delete'>Eliminar</button>
                    <Link to={`/jets/${jet.id}`}>
                      <button className='btn_edit'>Editar</button>
                    </Link>
                  </td>
                </tr>
              ))
            }
          </tbody>

        </table>
      </div>
    </div>
  )
}

export default JetsList
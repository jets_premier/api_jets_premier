import React from 'react'

import NavbarV from '../Components/Navbar-V'
import AdminForm from '../Components/AdminForm'

import './pages.css'

const Admin = () => {
  return (
    <div className='app_admin'>
      <NavbarV />
      <AdminForm />
    </div>
  )
}

export default Admin